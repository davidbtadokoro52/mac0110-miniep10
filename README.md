# MAC0110-MiniEP10

This is the Mini Programming Exercise 10 for the MAC0110 discipline
of Introduction to Computing. Following the trend of past MiniPEs the
focus of this project is to solve an algorithimic problem (the max sum
of a square submatrix) while also further learning the git mechanics.
