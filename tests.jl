include("miniep10.jl")
using Test

function testMaxSumSubMat()
	println("Começo dos testes.")
	@test max_sum_submatrix([]) == []
	@test max_sum_submatrix([1 1 1; 1 1 1; 1 1 1]) == [[1 1 1; 1 1 1; 1 1 1]]
	@test max_sum_submatrix([ 0 -1; -2 -3]) == [[0]]
	@test max_sum_submatrix([-1  1  1; 1 -1  1; 1  1 -2]) == [[-1 1 1; 1 -1 1; 1 1 -2], [1 1; -1 1], [1 -1; 1 1]]
	println("Fim dos testes.")
end

testMaxSumSubMat()
