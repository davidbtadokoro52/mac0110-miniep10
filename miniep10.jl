# Esta função cria uma submatriz

function make_submatrix(matrix, i, j, tam)
	if tam == 1
		submatrix = []
		push!(submatrix, matrix[i, j])
		return submatrix
	end
	submatrix = zeros(Int, tam, tam)
	for a in i:i + tam - 1
		for b in j: j + tam - 1
			submatrix[a - i + 1, b - j + 1] = matrix[a, b]
		end
	end
	return submatrix
end

#########################################

# Esta função retorna a soma de uma matriz

function sum_matrix(matrix)
	if matrix == []
		return 0
	end
	sum = 0
	for a in 1:length(matrix)
		sum = sum + matrix[a]
	end
	return sum
end

#########################################

# Esta função retorna um array com
# a(s) submatriz(zes) de soma máxima

function max_sum_submatrix(matrix)
	if matrix == []
		return []
	end
	TAM = size(matrix)[1]
	max_sub = []
	for i in 1:TAM
		for j in 1:TAM
			tam = TAM
			while tam >= 1
				if j + tam - 1 <= TAM && i + tam - 1 <= TAM
					submatrix = make_submatrix(matrix, i, j, tam)
					if max_sub == []
						push!(max_sub, submatrix)
					elseif sum_matrix(submatrix) == sum_matrix(max_sub[1])
						push!(max_sub, submatrix)
					elseif sum_matrix(submatrix) > sum_matrix(max_sub[1])
						max_sub = []
						push!(max_sub, submatrix)
					end
				end
				tam = tam - 1
			end
		end
	end
	return max_sub
end
